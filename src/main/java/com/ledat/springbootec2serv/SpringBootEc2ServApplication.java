package com.ledat.springbootec2serv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootEc2ServApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootEc2ServApplication.class, args);
    }

}
