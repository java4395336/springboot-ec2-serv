package com.ledat.springbootec2serv.rest;


import com.ledat.springbootec2serv.domain.Commune;
import com.ledat.springbootec2serv.service.CommuneService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/commune")
public class CommuneRestController extends MyRestController<Commune, Long> {
    public CommuneRestController(CommuneService service) {
        super(service);
    }
}
