package com.ledat.springbootec2serv.rest;

import com.ledat.springbootec2serv.service.MyService;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

public abstract class MyRestController<T extends Serializable, Idt extends Long> {
    protected final MyService<T, Idt> service;
    public MyRestController(MyService<T, Idt> service) {
        this.service = service;
    }

    @PostMapping("create")
    public T create(@RequestBody T value) {
        return service.create(value);
    }

    @GetMapping("getAll")
    public List<T> getAll() {
        return service.getAll();
    }

    @GetMapping("getById/{id}")
    public T getById(@PathVariable("id") Idt id) {
        return service.getById(id);
    }

    @PutMapping("update")
    public T update(@RequestBody T value) {
        return service.update(value);
    }

    @DeleteMapping("delete/{id}")
    public Boolean delete(@PathVariable("id") Idt id) {
        T value = service.getById(id);
        if (value == null) {
            return false;
        }
        return service.delete(value);
    }

}
