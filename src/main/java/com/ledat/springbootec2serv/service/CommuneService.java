package com.ledat.springbootec2serv.service;


import com.ledat.springbootec2serv.domain.Commune;

public interface CommuneService extends MyService<Commune, Long> {
}
