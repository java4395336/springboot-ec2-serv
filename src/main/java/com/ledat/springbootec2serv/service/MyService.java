package com.ledat.springbootec2serv.service;

import java.util.List;

public interface MyService<T, IdT extends Long>{
    T create(T value);
    T update(T value);
    List<T> getAll();
    T getById(IdT id);
    Boolean delete(T value);
}
