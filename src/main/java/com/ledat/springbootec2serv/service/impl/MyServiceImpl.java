package com.ledat.springbootec2serv.service.impl;

import com.ledat.springbootec2serv.repository.MyJpaRepository;
import com.ledat.springbootec2serv.service.MyService;

import java.io.Serializable;
import java.util.List;

public class MyServiceImpl<T extends Serializable, Idt extends Long> implements MyService<T, Idt> {
    protected final MyJpaRepository<T, Idt> repository;

    public MyServiceImpl(MyJpaRepository<T, Idt> repository) {
        this.repository = repository;
    }

    @Override
    public T create(T value) {
        return repository.save(value);
    }

    @Override
    public T update(T value) {
        return repository.save(value);
    }

    @Override
    public List<T> getAll() {
        return repository.findAll();
    }

    @Override
    public T getById(Idt id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Boolean delete(T value) {
        try {
            repository.delete(value);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
