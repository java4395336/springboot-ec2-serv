package com.ledat.springbootec2serv.service.impl;

import com.ledat.springbootec2serv.domain.Commune;
import com.ledat.springbootec2serv.repository.CommuneRepository;
import com.ledat.springbootec2serv.service.CommuneService;
import org.springframework.stereotype.Service;

@Service
public class CommuneServiceImpl extends MyServiceImpl<Commune, Long> implements CommuneService {
    public CommuneServiceImpl(CommuneRepository repository) {
        super(repository);
    }

}
