package com.ledat.springbootec2serv.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface MyJpaRepository<T, Idt extends Serializable> extends JpaRepository<T, Idt> {
}