package com.ledat.springbootec2serv.repository;

import com.ledat.springbootec2serv.domain.Commune;
import org.springframework.stereotype.Repository;

@Repository
public interface CommuneRepository extends MyJpaRepository<Commune, Long> {
}
